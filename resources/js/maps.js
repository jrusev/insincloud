    mapboxgl.accessToken = 'pk.eyJ1IjoianVsaWVucnVzZXYiLCJhIjoiY2p5NWd3a2s5MDZ1OTNscTA4eGI1djZxOSJ9.FcbN_C2ARF99y_JxW3ORZQ';
    var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v10',
    zoom: 16,
    center: [23.318530579102173, 42.69399156797948]
    });
    var map1 = new mapboxgl.Map({
    container: 'map1',
    style: 'mapbox://styles/mapbox/light-v10',
    zoom: 15,
    center: [27.92517271175302, 43.20918250902585]
    });

    var size = 120;
     
    var pulsingDot = {
    width: size,
    height: size,
    data: new Uint8Array(size * size * 4),
     
    onAdd: function() {
    var canvas = document.createElement('canvas');
    canvas.width = this.width;
    canvas.height = this.height;
    this.context = canvas.getContext('2d');
    },
     
    render: function() {
    var duration = 1000;
    var t = (performance.now() % duration) / duration;
     
    var radius = size / 2 * 0.3;
    var context = this.context;
     
     
    // draw inner circle
    context.beginPath();
    context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
    context.fillStyle = 'rgba(69, 159, 237, 1)';
    context.strokeStyle = 'white';
    context.lineWidth = 2 + 4 * (1 - t);
    context.fill();
    context.stroke();
     
    // update this image's data with data from the canvas
    this.data = context.getImageData(0, 0, this.width, this.height).data;
     
    // keep the map repainting
    map.triggerRepaint();
     
    // return `true` to let the map know that the image was updated
    return true;
    }
    };
     
    map.on('load', function () {
     
    map.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });
     
    map.addLayer({
    "id": "points",
    "type": "symbol",
    "source": {
    "type": "geojson",
    "data": {
    "type": "FeatureCollection",
    "features": [{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [23.3185, 42.6938]
    }
    }]
    }
    },
    "layout": {
    "icon-image": "pulsing-dot"
    }
    });
    });

    map1.on('load', function () {
     
    map1.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });
     
    map1.addLayer({
    "id": "points",
    "type": "symbol",
    "source": {
    "type": "geojson",
    "data": {
    "type": "FeatureCollection",
    "features": [{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [27.9255, 43.2090]
    }
    }]
    }
    },
    "layout": {
    "icon-image": "pulsing-dot"
    }
    });
    });