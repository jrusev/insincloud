$( document ).ready( function() {
    var value = $('#users-range').attr('data-slider-value');
    var separator = value.indexOf(',');
    if( separator !== -1 ){
      value = value.split(',');
      value.forEach(function(item, i, arr) {
        arr[ i ] = parseFloat( item );
      });
    } else {
      value = parseFloat( value );
    }
    $( '#users-range' ).slider({
      formatter: function(value) {
        $('#ex1Slider>.slider-handle').html(value);
        return value;
      },
      min: parseFloat( $( '#users-range' ).attr('data-slider-min') ),
      max: parseFloat( $( '#users-range' ).attr('data-slider-max') ), 
      range: $( '#users-range' ).attr('data-slider-range'),
      value: value,
      tooltip_split: $( '#users-range' ).attr('data-slider-tooltip_split'),
      tooltip: $( '#users-range' ).attr('data-slider-tooltip')
    });

    $('.changeCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $(".carousel").carousel("next");
    });
 } );

$( document ).ready( function() {
    var value = $('#products-range').attr('data-slider-value');
    var separator = value.indexOf(',');
    if( separator !== -1 ){
      value = value.split(',');
      value.forEach(function(item, i, arr) {
        arr[ i ] = parseFloat( item );
      });
    } else {
      value = parseFloat( value );
    }
    $( '#products-range' ).slider({
      formatter: function(value) {
        $('#ex2Slider>.slider-handle').html(value);
        return value;
      },
      min: parseFloat( $( '#products-range' ).attr('data-slider-min') ),
      max: parseFloat( $( '#products-range' ).attr('data-slider-max') ), 
      range: $( '#products-range' ).attr('data-slider-range'),
      value: value,
      tooltip_split: $( '#products-range' ).attr('data-slider-tooltip_split'),
      tooltip: $( '#products-range' ).attr('data-slider-tooltip')
    });
 } );

$(document).ready(function(){
  if (jQuery(window).width() <= 768) {
    $('.standart-label').addClass('active-package');

    $('.standart-label').click(function(){
      $('.standart-package').css('display', 'block');
      $('.pro-package').css('display', 'none');
      $('.pro-plus--package').css('display', 'none');

      $('.standart-label').addClass('active-package');
      $('.pro-label').removeClass('active-package');
      $('.pro-plus--label').removeClass('active-package');
    });

    $('.pro-label').click(function(){
      $('.standart-package').css('display', 'none');
      $('.pro-package').css('display', 'block');
      $('.pro-plus--package').css('display', 'none');

      $('.standart-label').removeClass('active-package');
      $('.pro-label').addClass('active-package');
      $('.pro-plus--label').removeClass('active-package');
    });

    $('.pro-plus--label').click(function(){
      $('.standart-package').css('display', 'none');
      $('.pro-package').css('display', 'none');
      $('.pro-plus--package').css('display', 'block');

      $('.standart-label').removeClass('active-package');
      $('.pro-label').removeClass('active-package');
      $('.pro-plus--label').addClass('active-package');
    });
  }
});

$(window).on("orientationchange",function(){
    if(window.orientation == 0)
    {
      $('.standart-label').addClass('active-package');
      $('.pro-descr').css('display', 'none');
      $('.pro-plus--descr').css('display', 'none');
      $('.pro-package').css('display', 'none');
      $('.pro-plus--package').css('display', 'none');

      $('.standart-label').click(function(){
        $('.standart-package').css('display', 'block');
        $('.pro-package').css('display', 'none');
        $('.pro-plus--package').css('display', 'none');

        $('.standart-label').addClass('active-package');
        $('.pro-label').removeClass('active-package');
        $('.pro-plus--label').removeClass('active-package');
      });

      $('.pro-label').click(function(){
        $('.standart-package').css('display', 'none');
        $('.pro-package').css('display', 'block');
        $('.pro-plus--package').css('display', 'none');

        $('.standart-label').removeClass('active-package');
        $('.pro-label').addClass('active-package');
        $('.pro-plus--label').removeClass('active-package');

         $('.pro-descr').css('display', 'block');
      });

      $('.pro-plus--label').click(function(){
        $('.standart-package').css('display', 'none');
        $('.pro-package').css('display', 'none');
        $('.pro-plus--package').css('display', 'block');

        $('.standart-label').removeClass('active-package');
        $('.pro-label').removeClass('active-package');
        $('.pro-plus--label').addClass('active-package');

        $('.pro-plus--descr').css('display', 'block');
      });
    }
    else
    {
      $('.standart-label').unbind('click');
      $('.pro-label').unbind('click');
      $('.pro-plus--label').unbind('click');
      $('.standart-package').css('display', 'block');
      $('.pro-package').css('display', 'block');
      $('.pro-plus--package').css('display', 'block');
      $('.pro-descr').css('display', 'block');
      $('.pro-plus--descr').css('display', 'block');

      $('.standart-label').removeClass('active-package');
      $('.pro-label').removeClass('active-package');
      $('.pro-plus--label').removeClass('active-package');
    }
  });

jQuery(function() {
  jQuery("#tabs").tabs({
    show: {duration: 1 }
  });
  jQuery( "#accordion" ).accordion();

  var btn = jQuery('#accordion li a');
  var wrapper = jQuery('#accordion li');

  jQuery(btn).on('click', function() {
    jQuery(btn).removeClass('active');
    jQuery(btn).parent().find('.addon').removeClass('fadein');
    
    jQuery(this).addClass('active');
    jQuery(this).parent().find('.addon').addClass('fadein');
  });
});

$(".tag").click(function(e) {
      e.preventDefault();
      e.stopPropagation();
      $('.slider_description div').fadeOut('slow');
      $('#' + $(this).data('rel')).fadeIn('slow');
});

let title_array = [
  '.title-1', '.title-2', 
  '.title-3', '.title-4', 
  '.title-5', '.title-6', 
  '.title-7', '.title-8',
  '.title-9', '.title-10',
  '.title-11', '.title-12',
  '.title-13'
];

$.each(title_array, function(index, value){
  $(value).click(function(){
    $('.title').removeClass('title-active');
    $(value).addClass('title-active');
  });
});

$(".tag").click(function() {
  if (jQuery(window).width() <= 1200) {
      $('html, body').animate({
          scrollTop: $(".slider_description").offset().top
      }, 500);
  }
});

$('#name').bind('keypress', testInputNumbers);

function testInputNumbers(event) {
   var value = String.fromCharCode(event.which);
   var pattern = new RegExp(/[a-zåäö ]/i);
   return pattern.test(value);
}

$("#message, #subject, #company").keypress(function(event){
    var ew = event.which;
    if(ew == 32)
        return true;
    if(48 <= ew && ew <= 57)
        return true;
    if(65 <= ew && ew <= 90)
        return true;
    if(97 <= ew && ew <= 122)
        return true;
    if(1 <= ew && ew <= 8)
        return true;  
    return false;
});

/*$(".member").click(function() {
    $('html, body').animate({
        scrollTop: $(".who-are-we-right").offset().top
    }, 500);
});*/
/*$(".yasen").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/yasen.png)");
});
$(".violeta").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/violeta.png)");
});
$(".dimitar").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/dimitar.png)");
});
$(".mihaela").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/mihaela.png)");
});
$(".martin").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/martin.png)");
});
$(".aynor").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/aynor.png)");
});
$(".julien").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/julien.png)");
});
$(".daniel").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/daniel.png)");
});
$(".alex").click(function() {
    $('.who-are-we-right').css("background-image", "url(/resources/images/alex.png)");
});*/

let data = {};
data.usd = {'standartPrice': null, 'proPrice': null, 'proPlusPrice': null};
data.eur = {'standartPrice': 39, 'proPrice': 69, 'proPlusPrice': 99};
data.gbp = {'standartPrice': null, 'proPrice': null, 'proPlusPrice': null};

//Setup Fee
let fee = {};
fee.usd = {'standartFee': null, 'proFee': null, 'proPlusFee': null};
fee.eur = {'standartFee': 1999, 'proFee': 999, 'proPlusFee': 0};
fee.gbp = {'standartFee': null, 'proFee': null, 'proPlusFee': null};

let rates = [];

const Url='https://api.exchangeratesapi.io/latest?base=EUR&symbols=USD,GBP';
   $.ajax({
     url: Url,
     type:"GET",
     success: function(result){
        rates = result.rates;
        data.usd.standartPrice = data.eur.standartPrice * rates.USD;
        data.usd.proPrice = data.eur.proPrice * rates.USD;
        data.usd.proPlusPrice = data.eur.proPlusPrice * rates.USD;

        data.gbp.standartPrice = data.eur.standartPrice * rates.GBP;
        data.gbp.proPrice = data.eur.proPrice * rates.GBP;
        data.gbp.proPlusPrice = data.eur.proPlusPrice * rates.GBP;

        fee.usd.standartFee = fee.eur.standartFee * rates.USD;
        fee.usd.proFee = fee.eur.proFee * rates.USD;
        fee.usd.proPlusFee = fee.eur.proPlusFee * rates.USD;

        fee.gbp.standartFee = fee.eur.standartFee * rates.GBP;
        fee.gbp.proFee = fee.eur.proFee * rates.GBP;
        fee.gbp.proPlusFee = fee.eur.proPlusFee * rates.GBP;

        calculatePackagePrice();
     },
     error:function(error){
        console.log('Error ${error}');
        //To be implemented
     }
   });

function calculatePackagePrice() {
  let users = document.getElementById('users-range').value;
  let products = document.getElementById('products-range').value;
  let currency_mark = $('input[name="currency"]:checked').val();

  //calc(data[currency_mark].standartPrice, data[currency_mark].proPrice, data[currency_mark].proPlusPrice, users, products);
  let standartResult = (data[currency_mark].standartPrice * products) + (data[currency_mark].standartPrice * 0.5 * users);
  $('.per-month--standart').html(Math.ceil(standartResult));

  let proResult = (data[currency_mark].proPrice * products) + (data[currency_mark].proPrice * 0.5 * users);
  $('.per-month--pro').html(Math.ceil(proResult));

  let proPlusResult = (data[currency_mark].proPlusPrice * products) + (data[currency_mark].proPlusPrice * 0.5 * users);
  $('.per-month--proplus').html(Math.ceil(proPlusResult));
}
 
$(document).ready(function() { 
  $('input[name="currency"]').change(function(){
    let currency_mark = $(this).val();

    if(currency_mark == "usd"){
      $(".currency-mark").html("$");
      //Change setup fee
      $(".standart-mark--value").html(Math.ceil(fee[currency_mark].standartFee));
      $(".pro-mark--value").html(Math.ceil(fee[currency_mark].proFee));
      $(".proplus-mark--value").html(Math.ceil(fee[currency_mark].proPlusFee));
    }else if(currency_mark == "eur"){
      $(".currency-mark").html("€");
      //Change setup fee
      $(".standart-mark--value").html(Math.ceil(fee[currency_mark].standartFee));
      $(".pro-mark--value").html(Math.ceil(fee[currency_mark].proFee));
      $(".proplus-mark--value").html(Math.ceil(fee[currency_mark].proPlusFee));
    }else if(currency_mark == "gbp"){
      $(".currency-mark").html("£");
      //Change setup fee
      $(".standart-mark--value").html(Math.ceil(fee[currency_mark].standartFee));
      $(".pro-mark--value").html(Math.ceil(fee[currency_mark].proFee));
      $(".proplus-mark--value").html(Math.ceil(fee[currency_mark].proPlusFee));
    }
      calculatePackagePrice();
  });
});

let linked_sections = {
  '.features-link': '#tabs',
  '.benefits-link': '.benefits',
  '.plan-link': '.plan',
  '.see_plans': '.plan',
  '.about-us--link': '.who-are-we-section',
  '.contact-us--link': '.contact_us',
}

$.each(linked_sections, function(key, value){
  $(key).click(function() {
    $('html, body').animate({
        scrollTop: $(value).offset().top
    }, 500);
  });
});

$(function () {    
  $("#contact-form1").submit(function (e) {
    e.preventDefault();
    var form_data = $(this).serialize(); 
    $.ajax({
      type: "POST", 
      url: "../../mail.php",
      dataType: "json", // Add datatype
      data: form_data
    }).done(function (data) {
      $('.success_message').css("display", "block");
      $('.wrap-input').css("display", "none");
      $('.form-btn').css("display", "none");
      $('form :input').val('');
    }).fail(function (data) {
      $('.error_message').css("display", "block");
      $('.wrap-input').css("display", "none");
      $('.form-btn').css("display", "none");
      $('form :input').val('');
    });
  }); 
});

$('.info-icon').on('touchstart', function () {
    $(this).trigger('hover');
}).on('touchend', function () {
    $(this).trigger('hover');
});